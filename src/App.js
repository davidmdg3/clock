import React from 'react';
import Simple from './Simple';
import RestApi from './RestApi';
import Clock from './Clock';

function App() {
  return (
    <div>
      <Simple name="David" apelido="Gonçalves" />
      <Clock title='Time'/>
      <RestApi />
    </div>
  );
}

export default App;
