import React, { Component } from 'react';

const API= process.env.REACT_APP_REST_API + "latest/"
export default class RestApi extends Component {

    constructor(props) {
        super()
        this.state = {
            rates: null
        }
    }
    //montar
    componentDidMount() {
        this.intervalID = setInterval(
            () => this.tick(), 6000)
    }

    tick() {
        console.log(API);
        //para não alterar o valor global API
        let query = API
        //faz fetch no url, quando houver dados envia em json
        fetch(query)
        .then(response => response.json())
        .then(data => this.setState({rates: data['rates']['USD']}))
        console.log(this.state.rates);
    }

    render() {
        return (
        <h2>{this.state.rates}</h2>
        )
    }

}