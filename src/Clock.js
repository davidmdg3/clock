import React, {Component} from 'react';

export default class Clock extends Component{

    constructor(props) {
        super()
        //variaveis a guardar, parecido a um array
        //guardamos tudo o que queremos
        this.state = { time: new Date().toLocaleString('sv-SE')}
        this.tick();
        
    }

    componentDidMount() {
        console.log('mounted');
        this.intervalID = setInterval(
            () => this.tick(), 1000)
    }

    componentWillUnmount() {
            clearInterval(this.intervalID)
    }

    tick() {
        console.log(this.state.time);
        this.setState({ time: new Date().toLocaleString('sv-SE') })
        
    }

    render() {
        return (
            <div> {this.props.title} : {this.state.time}</div>
        )
    }
}